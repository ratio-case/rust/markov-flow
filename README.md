# Markov flow

Markov chain steady-state calculations with applications in clustering and sequencing.

# Changelog

## [0.3.2] - 2022-10-10

### Changed

- nalgebra dependency update to ^0.31.2.

## [0.3.1] - 2022-08-11

### Fixed

- MarkovFlow object now uses correctly parsed adjacency matrix instead of input.

## [0.3.0] - 2022-06-03

### Added

- Sequencing functionality. Exposed via `sequence()` or the `sequence` module.

## [0.2.0] - 2022-06-02

### Added

- Clustering functionality. Exposed via `cluster()` or the `cluster` module.

## [0.1.1] - 2022-06-01

### Changed

- Slight additions and improvements to the `MarkovFlow` struct.

## [0.1.0] - 2022-05-19

### Added

- Basic Markov flow calculations.
