/// Utility functions.
use crate::{DMatrix, DVector};

/// get_max_degree Calculates the maximum degree (number of nonzero entries) in the given matrix.
pub fn get_max_degree(mat: &DMatrix<f64>, columns: bool) -> usize {
    let (rows, cols) = mat.shape();
    let mut degrees = DVector::<usize>::zeros(rows);
    let imax = if columns { rows } else { cols };
    let jmax = if columns { cols } else { rows };
    for i in 0..imax {
        for j in 0..jmax {
            if mat[(i, j)] > 0.0 {
                degrees[(j)] = degrees[(j)] + 1;
            }
        }
    }
    degrees.max()
}

/// Prune matrix' values below a certain threshold.
pub fn prune_matrix(matrix: &mut DMatrix<f64>, threshold: f64) {
    let (rows, cols) = matrix.shape();
    for i in 0..rows {
        for j in 0..cols {
            if (matrix[(i, j)].abs() - threshold).abs() < f64::EPSILON {
                matrix[(i, j)] = 0.0;
            }
        }
    }
}

/// Normalize a matrix' columns.
pub fn normalize_matrix_columns(matrix: &mut DMatrix<f64>) {
    for (j, mut col) in matrix.column_iter_mut().enumerate() {
        let sum = col.sum();
        if sum.abs() < f64::EPSILON {
            col.fill(0.0);
            col[j];
        } else {
            col *= 1.0 / sum;
        }
    }
}

/// Raise matrix elements to a floating point power.
pub fn matrix_elementwise_power(matrix: &mut DMatrix<f64>, power: f64) {
    let (rows, cols) = matrix.shape();
    for i in 0..rows {
        for j in 0..cols {
            matrix[(i, j)] = matrix[(i, j)].powf(power);
        }
    }
}
